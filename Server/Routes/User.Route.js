//Get Dependencies
const express = require('express');
const router = express.Router();


//Middleware
var UserController = require('../Controllers/UserController');

router.post('/updateuser',UserController.UpdateUserProfile);
router.post('/createprofile',UserController.createprofile);
router.post('/login',UserController.Login);



module.exports = router;
