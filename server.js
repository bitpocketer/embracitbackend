//Get Dependencies
const express = require('express');
const app = express();

// const path = require('path');
const http = require('http');
var bodyParser = require('body-parser');
const mongoose = require('./server/Database/Database');




// Get our API routes
const usersApi = require('./server/Routes/User.Route');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Expose-Headers", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,Authorization');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

app.use('/api/user', usersApi);

app.use(express.static('./public'));

const port = process.env.PORT || '3002';
app.set('port', port);
const server = http.createServer(app);

server.listen(port, () => console.log(`API running on localhost:${port}`));